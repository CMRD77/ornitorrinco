<?php

add_action( 'init', 'athelas_register_comic' );
function athelas_register_comic() {
	$labels = array(
		"name" => "Comics",
		"singular_name" => "Comic",
		"menu_name" => "Comics",
		"all_items" => "All Comics",
		"add_new" => "Add new",
		"add_new_item" => "Add new Comic",
		"edit" => "Editar",
		"edit_item" => "Edit Comic",
		"new_item" => "New Comic",
		"view" => "view",
		"view_item" => "See Comic",
		"search_items" => "Search Comics",
		"not_found" => "Comics not found",
		"not_found_in_trash" => "Pomics not found in the trash",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Post type Comics",
		'taxonomies' => array('category_comics'),
		"public" => true,
		"show_ui" => true,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "comic", "with_front" => true ),
		"query_var" => true,
						"supports" => array( 'title', 'editor','post_tags' ),			
	);
	register_post_type( "comics", $args );

}// End of athelas_register_comic()

add_action( 'init', 'athelas_register_movie' );
function athelas_register_movie() {
	$labels = array(
		"name" => "Movies",
		"singular_name" => "Movie",
		"menu_name" => "Movies",
		"all_items" => "All Movies",
		"add_new" => "Add new",
		"add_new_item" => "Add new Movie",
		"edit" => "Editar",
		"edit_item" => "Edit Movie",
		"new_item" => "New Movie",
		"view" => "view",
		"view_item" => "See Movie",
		"search_items" => "Search Movies",
		"not_found" => "Movies not found",
		"not_found_in_trash" => "Movies not found in the trash",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Post type Movies",
		'taxonomies' => array('category_movies'),
		"public" => true,
		"show_ui" => true,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "comic", "with_front" => true ),
		"query_var" => true,
						"supports" => array( 'title', 'editor','post_tags' ),			
	);
	register_post_type( "movies", $args );

}// End of athelas_register_movie()

add_action( 'init', 'athelas_register_sound' );
function athelas_register_sound() {
	$labels = array(
		"name" => "Sound",
		"singular_name" => "Sound",
		"menu_name" => "Sound",
		"all_items" => "All Sounds",
		"add_new" => "Add new",
		"add_new_item" => "Add new Sound",
		"edit" => "Editar",
		"edit_item" => "Edit Sound",
		"new_item" => "New Sound",
		"view" => "view",
		"view_item" => "See Sound",
		"search_items" => "Search Sounds",
		"not_found" => "Sounds not found",
		"not_found_in_trash" => "Sounds not found in the trash",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Post type Sounds",
		'taxonomies' => array('category_Sounds'),
		"public" => true,
		"show_ui" => true,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "Sound", "with_front" => true ),
		"query_var" => true,
						"supports" => array( 'title', 'editor','post_tags' ),			
	);
	register_post_type( "sound", $args );

}// End of athelas_register_sound()
