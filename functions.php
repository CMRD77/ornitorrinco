<?php 
function enqueue_styles_and_scripts(){
	wp_enqueue_style('base', get_stylesheet_directory_uri().'/style.css');
	// Our own theme styles
	wp_enqueue_style('font-awesome', get_stylesheet_directory_uri().'/stylesheets/font-awesome.min.css');
	wp_enqueue_style('ie8', get_stylesheet_directory_uri().'/stylesheets/ie8.css');
	wp_enqueue_style('main-theme', get_stylesheet_directory_uri().'/stylesheets/main.css');	
	
	/******* SCRIPTS *******/
	// jquery
	wp_enqueue_script('jquery', get_template_directory_uri().'scripts/jquery.min.js', null, null, true );
	wp_enqueue_script('droppotron', get_template_directory_uri().'scripts/jquery.dropotron.min.js', array("jquery"), null, true );
	wp_enqueue_script('skel', get_template_directory_uri().'scripts/skel.min.js', array("jquery"), null, true );
	wp_enqueue_script('util', get_template_directory_uri().'scripts/util.js', array("jquery"), null, true );
	wp_enqueue_script('main', get_template_directory_uri().'scripts/main.js', array("jquery"), null, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts');
include("includes/functions/posttypes.php");